package br.com.itau.investimento.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import br.com.itau.investimento.model.Aplicacao;
import br.com.itau.investimento.model.Cliente;
import br.com.itau.investimento.model.Produto;
import br.com.itau.investimento.model.Resultado;
import br.com.itau.investimento.repository.AplicacaoRepository;

@Service
public class AplicacaoService {
	
	@Autowired
	private AplicacaoRepository aplicacaoRepository;

	@Autowired
	private ClienteService clienteService;
		
	@Autowired
	private ProdutoService produtoService;
	
	public Iterable<Aplicacao> obterAplicacao() {
		System.out.println("Chamaram o listar aplicacao");

		return aplicacaoRepository.findAll();
	}

	
	public void criarAplicacao(Long id, Aplicacao aplicacao) {
		Cliente cliente = clienteService.encontraOuDaErro(id);
		Produto produto = produtoService.encontraOuDaErro(aplicacao.getProduto().getId());
		
		aplicacao.setCliente(cliente);
		aplicacao.setProduto(produto);
		aplicacaoRepository.save(aplicacao);
		System.out.println("Chamaram o criar da aplicacao");
	}
	
	public Resultado simularAplicacao( Aplicacao aplicacao) {
		Resultado resultado = new Resultado();
		Produto produto = produtoService.encontraOuDaErro(aplicacao.getProduto().getId());
		aplicacao.setProduto(produto);		
		
		double valorFinal = Math.pow((aplicacao.getProduto().getRendimento() + 1), aplicacao.getMeses()) * aplicacao.getValor();
		System.out.println("Chamaram o simular da aplicacao: " + aplicacao.getProduto().getRendimento());
		
		resultado.setValorFinal(valorFinal);
		
		return resultado;	
		
	}

	public Aplicacao encontraOuDaErro(Long id) {
		Optional<Aplicacao> optional = aplicacaoRepository.findById(id);
		
		if(!optional.isPresent()) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Aplicacao não encontrado");
		}
		
		return optional.get();
	}

}
